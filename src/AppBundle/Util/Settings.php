<?php
/**
 * @package platform
 * @author Petru Szemereczki <petru.office92@gmail.com>
 * @since 1.0
 */

namespace AppBundle\Util;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zend\Json\Json;

/**
 * Class Settings
 * @package AppBundle\Util
 *
 * This class provides a layer between database stored site configuration and php objects.
 *
 * Config can be accessed via:
 * - object property, if name is valid
 * - array key, if key name is invalid
 */
class Settings
{
    private $_config;

    use ContainerAwareTrait;

    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);

        $results = $this->container
            ->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Settings')
            ->findAllSettings();

        /** @var \AppBundle\Entity\Settings $result */
        foreach ($results as $result) {
            $this->set($result->getName(), $result->getValue());
        }
    }

    private function validate(string $name)
    {
        $test = '/[a-zA-Z0-9_\.]/i';

        return preg_match($test, $name);
    }

    public function set(string $name, $value)
    {
        if (!$this->validate($name)) {
            throw new \InvalidArgumentException('Config name is invalid!');
        }

        $this->_config[$name] = Json::encode($value);
        return $this;
    }

    public function get(string $name, bool $raw = false)
    {
        if (!$this->exists($name)) {
            return null;
        }

        $return = $this->_config[$name];

        if (!$raw) {
            $return = Json::decode($return);
        }

        return $return;
    }

    public function getRaw(string $name)
    {
        return $this->get($name, true);
    }

    public function exists(string $name)
    {
        return isset($this->_config[$name]);
    }

    public function remove(string $name)
    {
        if ($this->exists($name)) {
            unset($this->_config[$name]);
        }

        return $this;
    }
}