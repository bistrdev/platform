<?php
/**
 * Created by PhpStorm.
 * User: hktr92
 * Date: 6/22/17
 * Time: 12:55 PM
 */

namespace AppBundle\Util;

class MomentFormatConverter
{
    private static $formatConvertRules = [
        // year
        'yyyy'   => 'YYYY',
        'yy'     => 'YY',
        'y'      => 'YYYY',
        // day
        'dd'     => 'DD',
        'd'      => 'D',
        // day of week
        'EE'     => 'ddd',
        'EEEEEE' => 'dd',
        // timezone
        'ZZZZZ'  => 'Z',
        'ZZZ'    => 'ZZ',
        // letter 'T'
        '\'T\''  => 'T',
    ];

    public function convert($format)
    {
        return strtr($format, self::$formatConvertRules);
    }
}