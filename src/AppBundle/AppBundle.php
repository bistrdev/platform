<?php

namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    const NEWS_ONE_COLS = 1;
    const NEWS_TWO_COLS = 2;
}
