<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ToolboxImportUsersCommand extends ToolboxAbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('toolbox:import:users')
            ->setDescription('Imports users from json sample.')
            ->setAliases(['toolbox:user:import'])
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $users = $this->readJsonData('user')->getJsonData();
        $um = $this->getContainer()->get('fos_user.user_manager');

        foreach ($users as $user) {
            $usr = $um->createUser();

            $usr->setId($user->id);
            $usr->setUsername($user->username);
            $usr->setUsernameCanonical($this->convertCase($user->username, MB_CASE_LOWER));
            $usr->setEmail($user->email);
            $usr->setEmailCanonical($this->convertCase($user->email, MB_CASE_LOWER));
            $usr->setPlainPassword($user->password);
            $usr->setPassword($this->encodePassword($usr, $usr->getPlainPassword()));
            $usr->setFullName($this->convertCase($user->full_name, MB_CASE_TITLE));

            $roles = [];
            if (preg_match('/(colaborator)/i', $user->group_roles)) {
                $roles[] = 'ROLE_COLAB';
            }

            if (preg_match('/(redactor|reporter)/i', $user->group_roles)) {
                $roles[] = 'ROLE_REDACTOR';
            }

            if (preg_match('/(director|sef)/i', $user->group_roles)) {
                $roles[] = 'ROLE_ADMIN';
            }

            if (preg_match('/(robot|sysop)/i', $user->group_roles)) {
                $roles[] = 'ROLE_ADMIN';
                $roles[] = 'ROLE_SYSOP';
            }

            $usr->setRoles($roles);
            $usr->setPosition($this->convertCase($user->group_roles, MB_CASE_TITLE));
            $usr->setEnabled(true);
            $usr->setIsOAuthUser(false);

            $um->updateUser($usr);

            $output->writeln("Created user: <comment>{$user->full_name}</comment>");
        }
    }

}
