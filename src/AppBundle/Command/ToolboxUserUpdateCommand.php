<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ToolboxUserUpdateCommand extends ToolboxAbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('toolbox:user:update')
            ->setDescription('Updates users per user.json file.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $um = $this->getContainer()->get('fos_user.user_manager');
        $dataUsers = $this->readJsonData('user')->getJsonData();
        $users = $um->findUsers();

        $buster = 0;
        $dataUsersCount = count($dataUsers);
        $dbUsersCount = count($users);
        $ignoredUsers = $dbUsersCount - $dataUsersCount;
        $output->writeln(sprintf("Updating <comment>%d</comment> users, <comment>%d</comment> in database, <comment>%d</comment> ignored.", $dataUsersCount, $dbUsersCount, $ignoredUsers));
        /** @var User $user */
        foreach ($users as $user) {
            foreach ($dataUsers as $info) {
                $user->setUsername($info->username);
                $user->setUsernameCanonical($this->convertCase($info->username, MB_CASE_LOWER));

                $user->setEmail($info->email);
                $user->setEmailCanonical($this->convertCase($info->email, MB_CASE_LOWER));

                $user->setPassword($this->encodePassword($user, $info->password));
                $user->setFullName($this->convertCase($info->fullName, MB_CASE_TITLE));

                $user->setEnabled(true);
                $user->setIsOAuthUser(false);


                $roles = [];
                $rules = [
                    'colaborator' => 'ROLE_COLAB',
                    'redactor|reporter' => 'ROLE_REDACTOR',
                    'director|sef' => 'ROLE_ADMIN',
                    'robot|sysop' => 'ROLE_ADMIN,ROLE_SYSOP',
                ];

                foreach ($rules as $rule => $role) {
                    preg_match("/({$rule})/i", $info->group_roles) ? $roles[] = $role : null;
                }

                $user->setRoles($roles);
                $user->setPosition($this->convertCase($info->group_roles, MB_CASE_TITLE));

                $um->updateUser($user);

                $output->writeln("[{$buster}] - updated user: <comment>{$info->full_name}</comment>");
                $buster++;
            }
        }

        $output->writeln("Users updated!");
    }

}
