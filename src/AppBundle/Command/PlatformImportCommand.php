<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * Class PlatformImportCommand
 * @package AppBundle\Command
 *
 * @deprecated
 */
class PlatformImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('platform:import')->setDescription('Imports all SQL into the database and adjusts some things.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->cleanDatabase($input, $output);
        $this->importUsers($input, $output);
        $this->importDatabase($input, $output);
        $output->writeln('<info>All SQL data was imported!</info>');
        $output->writeln('');
        $this->fixFullNameCaseConsistency($input, $output);
        $output->writeln('');
        $this->fixEncodePasswords($input, $output);
        $output->writeln('');
        $this->fixUpdateRoles($input, $output);
        $output->writeln('');
        $this->fixCategoryNameCaseConsistency($input, $output);
        $output->writeln('');
        $this->fixCategorySlug($input, $output);
        $output->writeln('');
        $output->writeln('<info>Command execution completed!</info>');
    }

    private function cleanDatabase(InputInterface $input, OutputInterface $output) {
        $output->writeln('dropping existent database');
        $drop = $this->getCommand('doctrine:schema:drop');
        $drop->run(new ArrayInput(['--force' => true]), $output);

        $output->writeln('creating the database');
        $create = $this->getCommand('doctrine:schema:create');
        $create->run($input, $output);

        $output->writeln('creating database tables');
        $drop = $this->getCommand('doctrine:schema:update');
        $drop->run(new ArrayInput(['--force' => true]), $output);
    }

    private function importUsers(InputInterface $input, OutputInterface $output)
    {
        $command = $this->getApplication()->find('toolbox:user:import');
        $command->run($input, $output);
    }

    private function importDatabase(InputInterface $input, OutputInterface $output)
    {
        $env = $this->getContainer()->getParameter('kernel.environment');
        $finder = new Finder();
        $finder->in($this->getContainer()->getParameter('kernel.root_dir') . '/import')
               ->files()
               ->name('*.sql')
               ->sortByName();

        foreach ($finder as $fileInfo) {
            $filename = substr($fileInfo->getBasename('.sql'), 3);
            if (preg_match('/_/', $fileInfo->getFilename())) {
                /**
                 * Steps over $filename:
                 * - converts characters to lower
                 * - replaces '_' (underscore) with ' ' (blank space)
                 * - executes an uppercase conversion to all words in string (Some Words)
                 * - replaces ' ' (blank space) with '' (no space)
                 */
                $className = str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($filename))));
            } else {
                /**
                 * In this case, $filename contains no underscore. We capitalize first letter only.
                 */
                $className = ucfirst($filename);
            }
            $output->writeln(sprintf('Importing data for <comment>%s</comment> entity (<comment>%s</comment> table)',
                $className, $filename));
            $import = $this->getCommand('doctrine:database:import');
            $import->run(new ArrayInput(['file' => $fileInfo->getRealPath()]), $output);
        }
    }

    private function getCommand($command)
    {
        return $this->getApplication()->find($command);
    }

    private function importArticles(InputInterface $input, OutputInterface $output)
    {
        $directory = $this->getContainer()->getParameter('kernel.root_dir') . '/import';

        $fs = new Filesystem();
        $finder = new Finder();

        $finder->files()->name('*.sql')->in($directory)->sortByName();

        foreach ($finder as $fileInfo) {
            $output->writeln(sprintf('- current file: <comment>%s</comment>', $fileInfo->getFilename()));
            $import = $this->getCommand('doctrine:database:import');
            $import->run(new ArrayInput(['file' => $fileInfo->getRealPath()]), $output);
        }
    }

    private function fixFullNameCaseConsistency(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Fixing full name case consistency...');
        $em = $this->getEntityManager();
        $user = $em->getRepository('AppBundle:User');

        foreach ($user->findAll() as $result) {
            $fullName = $this->convertCase($this->convertCase($result->getFullName(), MB_CASE_LOWER), MB_CASE_TITLE);

            $output->writeln(sprintf('- fixing full name consistency for <comment>%s</comment>', $fullName));
            $result->setFullName($fullName);

            $em->persist($result);
            $em->flush();
        }
    }

    private function getEntityManager()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    private function convertCase($string, $case = MB_CASE_TITLE)
    {
        return mb_convert_case($string, $case, 'UTF-8');
    }

    private function fixEncodePasswords(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Encoding user passwords...');
        $encoder = $this->getContainer()->get('security.password_encoder');
        $em = $this->getEntityManager();
        $user = $em->getRepository('AppBundle:User');

        foreach ($user->findAll() as $result) {
            $output->writeln(sprintf('- encoding password for <comment>%s</comment>', $result->getFullName()));
            $result->setPlainPassword($result->getPassword());
            $result->setPassword($encoder->encodePassword($result, $result->getPlainPassword()));

            $em->persist($result);
            $em->flush();
        }
    }

    private function fixUpdateRoles(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Generating user roles...');
        $em = $this->getEntityManager();
        $role = $em->getRepository('AppBundle:Role');

        foreach ($role->findAll() as $result) {
            $output->writeln(sprintf('- fixed role: <comment>%s</comment>', $result->getName()));
            $name = $result->getName();

            $roles[] = 'ROLE_USER';
            if (preg_match('/(colaborator)/i', $name)) {
                $roles[] = 'ROLE_COLAB';
            }

            if (preg_match('/(redactor|reporter)/i', $name)) {
                $roles[] = 'ROLE_REDACTOR';
            }

            if (preg_match('/(director|sef)/i', $name)) {
                $roles[] = 'ROLE_ADMIN';
            }

            if (preg_match('/(robot)/i', $name)) {
                $roles[] = 'ROLE_ADMIN';
                $roles[] = 'ROLE_SYSOP';
            }

            $result->setName($this->convertCase($this->convertCase($result->getName(), MB_CASE_LOWER), MB_CASE_TITLE));
            $result->setRole($roles);

            $em->persist($result);
            $em->flush();
            unset($roles, $name);
        }
    }

    private function fixCategoryNameCaseConsistency(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Fixing category name case consistency...');
        $em = $this->getEntityManager();
        $user = $em->getRepository('AppBundle:Category');

        foreach ($user->findAll() as $result) {
            $name = $this->convertCase($this->convertCase($result->getName(), MB_CASE_LOWER), MB_CASE_UPPER);

            $output->writeln(sprintf('- fixing category name consistency for <comment>%s</comment>', $name));
            $result->setName($name);

            $em->persist($result);
            $em->flush();
        }
    }

    private function fixCategorySlug(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Fixing category slug consistency...');
        $em = $this->getEntityManager();
        $user = $em->getRepository('AppBundle:Category');

        foreach ($user->findAll() as $result) {
            $output->writeln(sprintf('- fixing category slug for <comment>%s</comment>', $result->getName()));
            $result->setSlug($result->getName());

            $em->persist($result);
            $em->flush();
        }
    }

    private function fixArticleSlug(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getEntityManager();
        $repo = $em->getRepository('AppBundle:Article');

        $output->writeln('Fixing article slug, please wait...');
        $counter = 0;
        foreach ($repo->findAll() as $result) {
            if ($result->getId() == 1) {
                $output->write($result->getId() . '...');
            }
            if ($result->getId() % 100 == 0) {
                $output->write($result->getId());
                if ($counter != $result->getId()) {
                    $output->write('...');
                }
            }

            $result->setSlug($result->getTitle());
            $result->setUpdatedAt(new \DateTime());

            $em->persist($result);
            $em->flush();
            $counter++;
        }

        $output->writeln('');
    }
}
