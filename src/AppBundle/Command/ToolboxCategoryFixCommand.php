<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ToolboxCategoryFixCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('toolbox:category:fix')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    private function getEntityManager()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->fixCategoryNameCaseConsistency($input, $output);
        $this->fixCategorySlug($input, $output);

        $output->writeln('Command result.');
    }

    private function fixCategoryNameCaseConsistency(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Fixing category name case consistency...');
        $em = $this->getEntityManager();
        $user = $em->getRepository('AppBundle:Category');

        foreach ($user->findAll() as $result) {
            $name = $this->convertCase($this->convertCase($result->getName(), MB_CASE_LOWER), MB_CASE_UPPER);

            $output->writeln(sprintf('- fixing category name consistency for <comment>%s</comment>', $name));
            $result->setName($name);

            $em->persist($result);
            $em->flush();
        }
    }

    private function fixCategorySlug(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Fixing category slug consistency...');
        $em = $this->getEntityManager();
        $user = $em->getRepository('AppBundle:Category');

        foreach ($user->findAll() as $result) {
            $output->writeln(sprintf('- fixing category slug for <comment>%s</comment>', $result->getName()));
            $result->setSlug($result->getName());

            $em->persist($result);
            $em->flush();
        }
    }
}
