<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ToolboxUpdateLegacyCodeCommand extends ToolboxAbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('toolbox:update:legacy-code')
            ->setDescription('Updates existent articles with legacy article id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = json_decode(file_get_contents($this->getContainer()->getParameter('kernel.root_dir') . '/data/article.json'));

        $pdo = $this->getDoctrine()->getConnection();
        foreach ($data as $key => $value) {
            try {
                $legacy_code = $value->legacy_code;
                $id = $value->id;

                $pdo->query("UPDATE article SET legacy_code = '{$legacy_code}' WHERE id = '{$id}'");
                $output->writeln("<info>updated: {$id}</info>");
            } catch (\PDOException $e) {
                $output->writeln("<error>failed: {$id}</error>");
            }
        }

        $output->writeln('done.');
    }

}
