<?php

namespace AppBundle\Command;

use AppBundle\Entity\Article;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ToolboxUpdateArticleSlugCommand extends ToolboxAbstractCommand
{
    protected function configure()
    {
        $this->setName('toolbox:update:article-slug')->setDescription('Fix articles slug');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $repo = $em->getRepository('AppBundle:Article');

        if (empty($repo)) {
            $output->writeln('<info>No articles to get slugged!</info>');

            return;
        }

        $pdo = $this->getDoctrine()->getConnection();
        /** @var Article $result */
        foreach ($repo->findAllSlugless() as $result) {
            $output->write($result->getId() . ' ');
            $article_id = Uuid::uuid4()->toString();
            $pdo->query("UPDATE article SET article_id = '{$article_id}' WHERE id = '{$result->getId()}'");
        }

        $output->writeln('');
        $output->writeln('<info>All articles were slugged!</info>');
    }

}
