<?php
/**
 * @package platform
 * @author Petru Szemereczki <petru.office92@gmail.com>
 * @since 1.0
 */

namespace AppBundle\Command;

use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ToolboxGenerateArticleIdCommand extends ToolboxAbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('toolbox:article:set-article-id')
            ->setDescription('Updates all articles from DB to have article ID.')
        ;
    }

    private function getEntityManager()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getEntityManager();
        $articles = $em->getRepository('AppBundle:Article');

        $results = $articles->createQueryBuilder('a')
            ->orderBy('a.updatedAt', 'DESC')
            ->addOrderBy('a.addedAt', 'DESC')
            ->getQuery()
            ->getResult();

        if (empty($results)) {
            $output->writeln('Nothing to do here!');
            exit(0);
        }

        foreach ($results as $result) {
            $result->setArticleId((Uuid::uuid4())->toString());

            $em->persist($result);
            $em->flush();

            $output->write("{$result->getId()}...");
        }

        $output->writeln('DONE!');
        $output->writeln('Command executed.');
    }
}