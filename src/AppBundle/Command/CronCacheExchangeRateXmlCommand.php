<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CronCacheExchangeRateXmlCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('cron:cache-nbr-rate')
            ->setDescription('Caches BNR exchange rate XML data.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $destination = dirname($this->getContainer()->getParameter('kernel.cache_dir'), 2) . '/nbr_exchange_rate.json';
        $request = $this->getContainer()->get('exchange_rate');

        $result = file_put_contents($destination, $request->getJsonResponse());

        if ((bool) $result) {
            $output->writeln("NBR exchange rate data cached in <comment>{$destination}</comment>");
        } else {
            $output->writeln("Failed to cache data!");
        }
    }
}
