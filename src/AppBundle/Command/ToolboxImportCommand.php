<?php
/**
 * @package platform
 * @author Petru Szemereczki <petru.office92@gmail.com>
 * @since 1.0
 */

namespace AppBundle\Command;


use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Process\Process;

class ToolboxImportCommand extends ToolboxAbstractCommand
{
    public function configure()
    {
        $this->setName('toolbox:import')->setDescription('Imports the basic structure for database usage.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $finder = new Finder();
        $fs = new Filesystem();
        $finder->in($this->getContainer()->getParameter('kernel.root_dir') . '/Import')
            ->files()
            ->name('*.sql')
            ->sortByName();

        foreach ($finder as $fileInfo) {
            $compressedSql = $fileInfo->getRealPath() . '.xz';
            $rawSql = $fileInfo->getRealPath();
            if ($fs->exists($compressedSql) && !$fs->exists($rawSql)) {
                $unxz = new Process("unxz --keep {$compressedSql}");
                $unxz->run();
            }

            $filename = substr($fileInfo->getBasename('.sql'), 3);
            if (preg_match('/_/', $filename)) {
                /**
                 * Steps over $filename:
                 * - converts characters to lower
                 * - replaces '_' (underscore) with ' ' (blank space)
                 * - executes an uppercase conversion to all words in string (Some Words)
                 * - replaces ' ' (blank space) with '' (no space)
                 */
                $className = str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($filename))));
            } else {
                /**
                 * In this case, $filename contains no underscore. We capitalize first letter only.
                 */
                $className = ucfirst($filename);
            }
            $output->writeln(sprintf('Importing data for <comment>%s</comment> entity (<comment>%s</comment> table)',
                $className, $filename));
            $import = $this->getApplication()->find('doctrine:database:import');
            $import->run(new ArrayInput(['file' => $rawSql]), $output);
        }
    }
}