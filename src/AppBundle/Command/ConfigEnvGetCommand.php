<?php
/**
 * @package platform
 * @author Petru Szemereczki <petru.office92@gmail.com>
 * @since 1.0
 */

namespace AppBundle\Command;


use AppBundle\Util\DotenvLoader;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConfigEnvGetCommand extends ToolboxAbstractCommand
{
    private $env;
    public function configure()
    {
        $this->setName('config:env:get')->setDescription('Gets an .env value, or dumps all values if no param defined.');
        $this->addArgument('key', InputArgument::OPTIONAL, 'Name of the env variable.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('key');
        $table = new Table($output);

        $table->setHeaders(['Key', 'Value']);
        $this->parseEnvFile();
        $table->render();
    }

    private function parseEnvFile()
    {
        $loader = $this->getContainer()->get(DotenvLoader::class);

        $lines = $loader->load();
        foreach ($lines as $line) {
            if ($loader->isComment($line)) {
                continue;
            }

            if ($loader->isSetter($line)) {
                list($name, $value) = $loader->sanitize($line);
                $this->env[$name] = $value;
            }
        }
    }
}