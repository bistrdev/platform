<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ToolboxUserGetRolesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('toolbox:user:get-roles')
            ->setDescription('Dumps user roles from database.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userManager = $this->getContainer()->get('fos_user.user_manager');

        foreach ($userManager->findUsers() as $user) {
            $output->writeln(sprintf("- user <comment>%s</comment> has role(s): <comment>%s</comment>", $user->getFullname(), implode(', ', $user->getRoles())));
        }
    }

}
