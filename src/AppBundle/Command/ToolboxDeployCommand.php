<?php
/**
 * @package platform
 * @author Petru Szemereczki <petru.office92@gmail.com>
 * @since 1.0
 */

namespace AppBundle\Command;


use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ToolboxDeployCommand extends ToolboxAbstractCommand
{
    private $commands = [
        'toolbox:import',
        'toolbox:user:update',
        'toolbox:article:set-article-id',
        'toolbox:update:article-slug',
        'toolbox:update:legacy-code',
    ];

    public function configure()
    {
        $this->setName('toolbox:deploy')->setDescription('Executes a list of commands for application deployment.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->commands as $command) {
            $runner = $this->getApplication()->find($command);
            $runner->run($input, $output);

            $output->writeln("Command execution: <comment>{$runner->getName()}</comment>");
        }
    }
}