<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle;

use Composer\Script\Event;
use Symfony\Component\Process\Process;

/**
 * @author Petru Szemereczki
 * Forked from SensioDistributionBundle with more features
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class ScriptHandler
{
    /**
     * Composer variables are declared static so that an event could update
     * a composer.json and set new options, making them immediately available
     * to forthcoming listeners.
     */
    protected static $options = array(
        'symfony-app-dir'        => 'app',
        'symfony-web-dir'        => 'web',
        'symfony-assets-install' => 'hard',
        'symfony-cache-warmup'   => false,
    );

    /**
     * Clears the Symfony cache.
     * @param Event $event
     */
    public static function clearCache(Event $event)
    {
        $options = static::getOptions($event);
        $consoleDir = static::getConsoleDir($event, 'clear the cache');

        if (null === $consoleDir) {
            return;
        }

        static::executeCommand($event, $consoleDir, 'cache:clear --no-warmup', $options['process-timeout']);
    }

    /**
     * Warms up the Symfony cache
     * @param Event $event
     */
    public static function warmupCache(Event $event)
    {
        $options = static::getOptions($event);
        $consoleDir = static::getConsoleDir($event, 'warm up the cache');

        if (null === $consoleDir) {
            return;
        }

        static::executeCommand($event, $consoleDir, 'cache:warmup', $options['process-timeout']);
    }

    protected static function getOptions(Event $event)
    {
        $options = array_merge(static::$options, $event->getComposer()->getPackage()->getExtra());

        $options['symfony-assets-install'] = getenv('SYMFONY_ASSETS_INSTALL') ?: $options['symfony-assets-install'];
        $options['symfony-cache-warmup'] = getenv('SYMFONY_CACHE_WARMUP') ?: $options['symfony-cache-warmup'];

        $options['process-timeout'] = $event->getComposer()->getConfig()->get('process-timeout');
        $options['vendor-dir'] = $event->getComposer()->getConfig()->get('vendor-dir');

        return $options;
    }

    /**
     * Returns a relative path to the directory that contains the `console` command.
     * @param Event  $event      The command event
     * @param string $actionName The name of the action
     * @return string|null The path to the console directory, null if not found
     */
    protected static function getConsoleDir(Event $event, $actionName)
    {
        $options = static::getOptions($event);

        if (!static::hasDirectory($event, 'symfony-bin-dir', $options['symfony-bin-dir'], $actionName)) {
            return;
        }

        return $options['symfony-bin-dir'];
    }

    protected static function hasDirectory(Event $event, $configName, $path, $actionName)
    {
        if (!is_dir($path)) {
            $event->getIO()->write(sprintf('The %s (%s) specified in composer.json was not found in %s, can not %s.',
                    $configName, $path, getcwd(), $actionName));

            return false;
        }

        return true;
    }

    protected static function executeCommand(Event $event, $consoleDir, $cmd, $timeout = 300)
    {
        $console = escapeshellarg($consoleDir.'/console');
        if ($event->getIO()->isDecorated()) {
            $console .= ' --ansi';
        }

        $process = new Process($console.' '.$cmd, null, null, null, $timeout);
        $process->run(function ($type, $buffer) use ($event) {
            $event->getIO()->write($buffer, false);
        });
        if (!$process->isSuccessful()) {
            throw new \RuntimeException(sprintf("An error occurred when executing the \"%s\" command:\n\n%s\n\n%s",
                escapeshellarg($cmd), self::removeDecoration($process->getOutput()),
                self::removeDecoration($process->getErrorOutput())));
        }
    }

    private static function removeDecoration($string)
    {
        return preg_replace("/\033\[[^m]*m/", '', $string);
    }

    /**
     * Installs the assets under the web root directory.
     * For better interoperability, assets are copied instead of symlinked by default.
     * Even if symlinks work on Windows, this is only true on Windows Vista and later,
     * but then, only when running the console with admin rights or when disabling the
     * strict user permission checks (which can be done on Windows 7 but not on Windows
     * Vista).
     * @param Event $event
     */
    public static function installAssets(Event $event)
    {
        $options = static::getOptions($event);
        $consoleDir = static::getConsoleDir($event, 'install assets');

        if (null === $consoleDir) {
            return;
        }

        $webDir = $options['symfony-web-dir'];

        $symlink = '';
        if ('symlink' == $options['symfony-assets-install']) {
            $symlink = '--symlink ';
        } elseif ('relative' == $options['symfony-assets-install']) {
            $symlink = '--symlink --relative ';
        }

        if (!static::hasDirectory($event, 'symfony-web-dir', $webDir, 'install assets')) {
            return;
        }

        static::executeCommand($event, $consoleDir, 'assets:install '.$symlink.escapeshellarg($webDir),
            $options['process-timeout']);
    }

    public static function createNginxLogsDirectory(Event $event)
    {
        $options = static::getOptions($event);
        $consoleDir = static::getConsoleDir($event, 'create nginx logs directory');

        if (null === $consoleDir) {
            return;
        }

        $varDir = $options['symfony-var-dir'];

        if (!is_dir("{$varDir}/nginx")) {
            if (!mkdir("{$varDir}/nginx")) {
                $event->getIO()->writeError('Unable to create nginx logs dir, possibly permissions error!');
            } else {
                $event->getIO()->write('Nginx logs dir created!');
            }
        }
    }
}
