<?php
/**
 * @package platform
 * @author Petru Szemereczki <petru.office92@gmail.com>
 * @since 1.0
 */

namespace AppBundle\EventSubscriber;


use AppBundle\Util\MaintenanceHandler;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class MaintenanceModeSubscriber implements EventSubscriberInterface
{
    /** @var MaintenanceHandler */
    protected $handler;

    public function __construct(ContainerInterface $container)
    {
        $this->handler = $container->get(MaintenanceHandler::class);
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $headers = $request->headers;

        $sysop = $headers->has('x-sysop-debugger') ? $headers->get('x-sysop-debugger') : null;

        if ($sysop == '100001607809751') {
            $request->attributes->set('maintenance.bypass', true);
            $request->cookies->set('__sysop_id', Uuid::uuid4()->toString());
        }

        if ($this->handler->isLocked() && HttpKernelInterface::MASTER_REQUEST === $event->getRequestType()) {
            $request->attributes->set('maintenance.enabled', true);
            throw new ServiceUnavailableHttpException(5, 'Mentenanta');
        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();

        $attributes = $request->attributes;
        $cookies = $request->cookies;

        if ($attributes->has('maintenance.bypass')) {
            if (!$cookies->has('__sysop_id')) {
                $cookie = new Cookie('__sysop_id', $cookies->get('__sysop_id'), new \DateTime('-1 day'));
                $response->headers->setCookie($cookie);
            }
        }

        if ($attributes->has('maintenance.enabled') && !$request->cookies->has('sysop_id')) {
            $response = $event->getResponse();
            $response->setStatusCode(503, 'Mentenanta');
        }
    }
}