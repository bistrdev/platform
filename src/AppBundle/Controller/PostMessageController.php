<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostMessageController extends Controller
{
    /**
     * @Route("/cereals/receiver")
     * @return Response
     */
    public function blablaAction()
    {
        $response = <<<EOV
<!DOCTYPE html>
<html>
    <head>
        <title>Receiver</title>
    </head>
    <body>
        <h1>Receiver</h1>
        <p>This page receives cereals from API with postMessage...</p>
        <hr>
        <iframe id="receiver" src="http://api.robery.eu/test/sender"></iframe>
    </body>
    
    <script type="text/javascript">
        window.addEventListener('message',function(e) {
            var key = e.message ? 'message' : 'data';
            var data = e[key];
            
            alert("Received data: " + data);
        }, false);
    </script>
</html>
EOV;

        return new Response($response, Response::HTTP_OK, ['Content-Type' => 'text/html']);
    }
}