<?php
/**
 * Created by PhpStorm.
 * User: hktr92
 * Date: 7/7/17
 * Time: 9:17 PM
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AccountController
 * @package AppBundle\Controller
 * @Route("/panou/cont")
 * @Security("is_granted('ROLE_USER')")
 */
class AccountController extends Controller
{

    /**
     * @Route("/reclamele-mele", name="panel_account_ads")
     */
    public function listAdsAction()
    {
        $ads = $this->getDoctrine()->getRepository('AppBundle:Ad')->findAllPaginatedFromUser($this->getUser());

        return $this->render('Platform/user_ads.html.twig', [
            'ads' => $ads,
        ]);
    }
}