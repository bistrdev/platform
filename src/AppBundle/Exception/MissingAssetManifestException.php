<?php
/**
 * @package platform
 * @author Petru Szemereczki <petru.office92@gmail.com>
 * @since 1.0
 */

namespace AppBundle\Exception;


class MissingAssetManifestException extends \RuntimeException
{

}