<?php
/**
 * @package platform
 * @author Petru Szemereczki <petru.office92@gmail.com>
 * @since 1.0
 */

namespace AppBundle\Twig\Extension;


use AppBundle\Util\Text;

class TextExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('remove_accents', [Text::class, 'removeAccents']),
        ];
    }
}