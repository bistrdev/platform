<?php

$options = getopt('pf', ['profile', 'force']);

function getOption($option)
{
    global $options;

    return isset($options[$option[0]]) || isset($options[$option]);
}

function profile()
{
    return getOption('profile');
}

function force()
{
    return getOption('force');
}

function getFontFamily($fontName)
{
    return explode('-', $fontName)[0];
}

function removeDirectory($path) {
    $files = glob($path . '/*');
    foreach ($files as $file) {
        $removed = is_dir($file) ? removeDirectory($file) : unlink($file);

        if (!$removed) {
            return false;
        }
    }

    rmdir($path);
    return true;
}

$baseUrl = 'https://a1.nyt.com/fonts/family';

$fontFamilies = [
    'cheltenham', 'cheltenham-sh',
    'franklin',
];

$types = [
    'normal', 'italic',
];

$sizes = [
    100, 200, 300, 400, 500, 600, 700, 800, 900
];

$extensions = [
    'ttf', 'woff',
];

$outputStyles = [
    'default' => "\x1B[0m",
    'error' => "\x1B[31m",
    'notice' => "\x1B[32m",
    'highlight' => "\x1B[33m"
];



$links = [];
foreach ($fontFamilies as $fontFamily) {
    foreach ($types as $type) {
        foreach ($sizes as $size) {
            foreach ($extensions as $extension) {
                $fontName = sprintf("%s-%s-%s.%s", $fontFamily, $type, $size, $extension);
                $links[$fontName] = sprintf("%s/%s/%s", $baseUrl, $fontFamily, $fontName);
            }
        }
    }
}

$outputDir = dirname(__DIR__) . '/var/downloaded-fonts';

if (force()) {
    if (removeDirectory($outputDir)) {
        echo "[-] removed output directory" . PHP_EOL;
    } else {
        echo "[/] unable to remove output directory!" . PHP_EOL;
    }
}

if (!is_dir($outputDir)) {
    $created = mkdir($outputDir);
    if ($created) {
        echo "[+] created output directory" . PHP_EOL;
    } else {
        echo "[/] unable to create output directory!" . PHP_EOL;
    }
}

$current = 1;
$total = count($links);
$downloaded = 0;
if (profile()) {
    $profileStart = microtime(true);
    echo "{$outputStyles['notice']}Script profiler is active!{$outputStyles['default']}" . PHP_EOL;
}

echo "Trying to download {$outputStyles['notice']}{$total}{$outputStyles['default']} fonts..." . PHP_EOL;
foreach ($links as $fontFile => $link) {
    $data = @file_get_contents($link);

    if (strlen($data) > 0) {
        $fontFamily = getFontFamily($fontFile);
        if (!is_dir($fontsDir = dirname(__DIR__) . '/var/downloaded-fonts/' . $fontFamily)) {
            if (mkdir($fontsDir)) {
                echo "[+] created font family directory: {$fontFamily}" . PHP_EOL;
            } else {
                echo "[-] failed to create font family directory: {$fontFamily}" . PHP_EOL;
            }
        }

        file_put_contents("{$outputDir}/{$fontFamily}/{$fontFile}", $data);
        $color = $outputStyles['notice'];
        $status = ' OK ';
        $downloaded++;
    } else {
        $color = $outputStyles['error'];
        $status = 'FAIL';
    }

    echo sprintf("[%s%s%s] [%d/%d] %s%s", $color, $status, $outputStyles['default'], $current, $total, $fontFile, PHP_EOL);
    $current++;
}

echo PHP_EOL;
echo "Total fonts: {$outputStyles['highlight']}{$total}{$outputStyles['default']} | Downloaded fonts: {$outputStyles['highlight']}{$downloaded}{$outputStyles['default']}" . PHP_EOL;

if (profile()) {
    $profileEnd = microtime(true);
    $profiledTime = ($profileEnd - $profileStart);
    $humanTime = date('H:i:s', $profiledTime);
    echo "Total execution time: {$outputStyles['highlight']}{$humanTime}{$outputStyles['default']}" . PHP_EOL;
}