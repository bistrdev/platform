#!/bin/bash

PHP_VERSION='7.0'
ROOT_PATH=$(dirname $(dirname $(realpath $0)))

function writeop
{
    local message=$1
    echo -e "\x1B[34m$message\x1B[0m"
}

function exec_php
{
    local php=

    if [[ ! -z $PHP_VERSION ]]; then
        php=$(which php${PHP_VERSION})
    else
        php=$(which php)
    fi


    ${php} -f $1
    return $?
}

function exec_console
{
    ./console $@
    return $?
}

function exec_check
{
    if [[ ! -e $(which php${PHP_VERSION}) ]]; then
        echo -e "\x1B[31mUnable to find any \x1B[33mPHP\x1B[31m installation!\x1B[0m"
        echo -e "\x1B[31mThe deployment script cannot continue due to missing dependencies.\x1B[0m"
        return 1
    fi

    if [[ ! -e symfony_requirements.patch ]]; then
        echo -e "\x1B[31mUnable to find patch for \x1B[33msymfony_requirements\x1B[31m!\x1B[0m"
        echo "\x1B[31mDeployment script will stop here due to missing patch.\x1B[0m"
        return 1
    fi

    if [[ ! -x symfony_requirements ]]; then
        echo -e "\x1B[31mYou can't execute \x1B[33msymfony_requirements\x1B[31m.\x1B[0m"
        echo -e "\x1B[31mPlease set the executable flag: \x1B[33mchmod +x symfony_requirements\x1B[0m"
        return 1
    fi

    if [[ ! -x console ]]; then
        echo -e "\x1B[31mYou can't execute console.\x1B[0m"
        echo -e "\x1B[31mPlease set the executable flag: \x1B[33mchmod +x console\x1B[0m"
        return 1
    fi

    return 0
}

function requirements_patch
{
    writeop "Checking if symfony_requirements has been patched already..."
    patch --dry-run -p0 -N --silent < symfony_requirements.patch 2>/dev/null
    if [[ $? -eq 0 ]]; then
        writeop "Patching symfony_requirements..."
        patch < symfony_requirements.patch
        if [[ $? -ne 0 ]]; then
            writeop "error: Failed to patch symfony_requirements"
            return $?
        fi

        writeop "success: symfony_requirements: patch applied"
        return 0
    fi

    writeop "Nothing to patch!"
    return 0
}

function requirements_check
{
    writeop "Executing Symfony requirements check"
    exec_php symfony_requirements
    if [[ $? -ne 0 ]]; then
        writeop "Something went wrong!"
        return $?
    fi

    if [[ -e ../var/incompatible.lock ]]; then
        cat ../var/incompatible.lock
        return 1
    fi

    writeop "Yay! Your system is ready to handle Symfony!"
    return 0
}

function database_create
{
    if [[ $1 == '--hard' ]]; then
        writeop "Dropping current database (if exists)"
        exec_console doctrine:database:drop --force
        if [[ $? -ne 0 ]]; then
            writeop "Database drop failed."
            return $?
        fi
        writeop "Database dropped."
    fi

    writeop "Creating the database"
    php console doctrine:database:create
    if [[ $? -ne 0 ]]; then
        writeop "Database creation failed"
        return $?
    fi
    writeop "Database created"

    return 0
}

function database_migrate
{
    writeop "Migrating database changes"
    exec_console doctrine:migration:migrate
    if [[ $? -ne 0 ]]; then
        writeop "Database migration failed."
        return $?
    fi
    writeop "Database changes migrated."

    return 0
}

function database_import {
    writeop "Importing SQL files..."
    exec_console platform:import
    if [[ $? -ne 0 ]]; then
        writeop "Database import failed!"
        return $?
    fi

    writeop "Database import finished!"
    return 0
}

function cache_clear
{
    writeop "Wiping the cache"
    exec_console cache:clear --no-warmup
    if [[ $? -ne 0 ]]; then
        writeop "Cache wipe failed."
        return $?
    fi

    writeop "Cache wiped"
    return 0
}

function cache_warmup {
    writeop "Warming up the cache"
    exec_console cache:warmup
    return $?
}

function assets_dump
{
    writeop "Dumping assets, please wait..."
    exec_console assetic:dump
    if [[ $? -ne 0 ]]; then
        writeop "Assets did not dump!"
        return $?
    fi

    writeop "Assets dumped without any issues."
    return 0
}

function assets_tinymce
{
    writeop "Dumping TinyMCE assets..."
    exec_console platform:tinymce
    if [[ $? -ne 0 ]]; then
        writeop "TinyMCE assets were not dumped!"
        return $?
    fi

    writeop "TinyMCE assets dumped without any issues."
    return 0
}

function dm_composer
{
    local op=
    if [[ -z $(which composer) ]]; then
        writeop "Unable to find Composer on this system!"
        writeop "Please install it: https://getcomposer.org/"
        writeop "and export it's location to PATH"
        return 1
    fi

    if [[ ! -e "${ROOT_PATH}/vendor" ]]; then
        op="install"
    fi

    writeop "Installing PHP dependencies..."
    composer install --working-dir=$ROOT_PATH
    if [[ $? -ne 0 ]]; then
        writeop "Composer installation failed!"
        return $?
    fi

    writeop "Composer execution completed!"
    return 0
}

function dm_bower {
        local op=
    if [[ -z $(which bower) ]]; then
        writeop "Unable to find Bower on this system!"
        writeop "Please install it: https://bower.io/"
        writeop "and export it's location to PATH"
        return 1
    fi

    if [[ ! -e "${ROOT_PATH}/web/assets/vendor" ]]; then
        op="install"
    fi

    writeop "Installing assets dependencies..."
    bower install
    if [[ $? -ne 0 ]]; then
        writeop "Bower installation failed!"
        return $?
    fi

    writeop "Bower execution completed!"
    return 0
}

function prod_enhance_autoload {
    writeop "Dumping an optimized auto-load for Composer"
    composer dump-autoload --optimize --no-dev --classmap-authoritative
    if [[ $? -ne 0 ]]; then
        writeop "Composer optimization failed!"
        return $?
    fi

    writeop "Composer optimization completed!"
    return 0
}

function main
{
    if [[ $1 != '--start' ]]; then
        writeop "You must confirm the deployment start point!"
        writeop "Please add the \x1B[33m--start\x1B[34m parameter"
        return 1
    fi

    echo -e "\x1B[32mDeploying Symfony application...\x1B[0m"
    exec_check

    requirements_patch
    requirements_check

    dm_bower
    dm_composer

    prod_enhance_autoload

    cache_clear
    cache_warmup

    database_import

    assets_dump
    assets_tinymce
    echo -e "\x1B[32mSymfony application deployed!\x1B[0m"
}

main $@
exit $?