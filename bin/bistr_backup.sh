#!/usr/bin/env bash

BACKUP_PATH=$(mktemp --directory)

# Generates a mysqldump file
function dump {
    local sql_file=$(mktemp --suffix=sql)
    local sum_file=$(mktemp --suffix=sha256sum)
}

# Creates an archive + checksum
function pack {
    local tar_file=$(mktemp --suffix=txz)
}

# Executes the program
function main {
    if [[ $1 == '--start' ]]; then
        return 1
    fi


}