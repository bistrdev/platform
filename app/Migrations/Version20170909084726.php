<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Import: Please modify to your needs!
 */
class Version20170909084726 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ad DROP type, CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE user_id user_id INT DEFAULT NULL, CHANGE image image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE article ADD article_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', ADD type VARCHAR(255) NOT NULL, DROP is_video, DROP is_hot, DROP is_recommended, DROP media_type, CHANGE section_id section_id INT DEFAULT NULL, CHANGE media media CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', CHANGE legacy_code legacy_code VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ad ADD type VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE id id INT NOT NULL, CHANGE user_id user_id INT DEFAULT NULL, CHANGE image image VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE article ADD is_video TINYINT(1) NOT NULL, ADD is_hot TINYINT(1) NOT NULL, ADD is_recommended TINYINT(1) NOT NULL, ADD media_type VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, DROP article_id, DROP type, CHANGE section_id section_id INT DEFAULT NULL, CHANGE legacy_code legacy_code VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE media media LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
