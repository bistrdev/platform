<?php

use Symfony\Component\HttpFoundation\Request;

function show_maintenance($message)
{
    $response =<<<EOR
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mentenanță</title>
    </head>
    <body>
        <h1>Mentenanță</h1>
        <p>Momentan, site-ul se află în mentenanță. Vă rugăm reveniți mai târziu!</p>
    </body>
</html>
EOR;

    error_log($message);
    echo $response;
    exit;
}

$autoload = dirname(__DIR__) . '/vendor/autoload.php';

if (!file_exists($autoload)) {
    show_maintenance('missing_vendor_directory');
}

$loader = require $autoload;

$env = new \Dotenv\Dotenv(dirname(__DIR__));
$env->load();

$environment = getenv('APP_ENV');
$maintenance = getenv('APP_MAINTENANCE');

if ($maintenance == 1) {
    show_maintenance('envvar_app_maintenance');
}

if ($environment == 'dev') {
    \Symfony\Component\Debug\Debug::enable();
}

$kernel = new AppKernel($environment, true);

if ($kernel->getEnvironment() == 'prod') {
    $kernel = new AppCache($kernel);
    Request::enableHttpMethodParameterOverride();
}

$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
