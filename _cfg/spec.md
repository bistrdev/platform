# Deployment

System requirements:
- OS: Modern Linux distro (CentOS 7, Ubuntu 16.04, Debian 8...):
    - [ ] libxml 2.6.21 or newer
    - [ ] icu 4 or newer
    - [ ] apc 3.0.17 or newer
- Apache 2.4:
    - [ ] mod_rewrite
    - [ ] mod_headers
- MariaDB 10.1 or newer:
    - [ ] utf8mb4 support
    - [ ] xtradb (if possible) or innodb
- Language: PHP 7.0 or newer:
    - [ ] pdo + pdo_mysql
    - [ ] json
    - [ ] ctype
    - [ ] php-xml
    - [ ] php tokenizer enabled
    - [ ] mbstring
    - [ ] iconv
    - [ ] posix
    - [ ] intl
    - [ ] redis
- Cache: Redis
